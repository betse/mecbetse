.. # ------------------( DIRECTIVES                         )------------------
.. # Fallback language applied to all code blocks failing to specify an
.. # explicit language. Since the majority of all code blocks in this document
.. # are Bash one-liners intended to be run interactively, this is "console".
.. # For a list of all supported languages, see also:
.. #     http://build-me-the-docs-please.readthedocs.org/en/latest/Using_Sphinx/ShowingCodeExamplesInSphinx.html#pygments-lexers

.. # FIXME: Sadly, this appears to be unsupported by some ReST parsers and is
.. # thus disabled until more widely supported. *collective shrug*
.. # highlight:: console

.. # ------------------( SYNOPSIS                           )------------------

========
mecBETSE
========

**mecBETSE** (**mec**\ hanical stress-strain **B**\ io\ **E**\ lectric **T**\
issue **S**\ imulation **E**\ ngine) is an official fork of BETSE_ – an
open-source cross-platform `discrete exterior calculus`_ simulator for 2D
computational multiphysics problems in the life sciences. mecBETSE calculates
supplementary properties of ongoing research interest, including:

- Mechanical stress and strain from osmotic cellular pressure.
- Feedback between stress-sensitive ion channels opened by cellular membrane
  tension circularly modifying osmotic cellular pressure.

mecBETSE is maintained by `Alessandro Leronni`_, `portably implemented
<codebase_>`__ in pure `Python 3`_, `continuously stress-tested <testing_>`__
with GitLab-CI_ **+** py.test_, and `permissively distributed <License_>`__
under the `BSD 2-clause license`_.

.. # ------------------( TABLE OF CONTENTS                  )------------------
.. # Blank line. By default, Docutils appears to only separate the subsequent
.. # table of contents heading from the prior paragraph by less than a single
.. # blank line, hampering this table's readability and aesthetic comeliness.

|

.. # Table of contents, excluding the above document heading. While the
.. # official reStructuredText documentation suggests that a language-specific
.. # heading will automatically prepend this table, this does *NOT* appear to
.. # be the case. Instead, this heading must be explicitly declared.

.. contents:: **Contents**
   :local:

.. # ------------------( DESCRIPTION                        )------------------

Installation
============

mecBETSE is readily installable with pip_, the standard Python package manager:

.. code-block:: console

   pip3 install mecbetse

License
=======

mecBETSE is open-source software `released <license_>`__ under the permissive
`BSD 2-clause license`_.

See Also
========

-  `BETSE`_, the actively maintained upstream project from which this
   downstream project was originally forked.

.. # ------------------( LINKS ~ betse                      )------------------
.. _BETSE:
   https://gitlab.com/betse/betse

.. # ------------------( LINKS ~ betsee                     )------------------
.. _BETSEE:
   https://gitlab.com/mecbetse/betsee
.. _BETSEE codebase:
   https://gitlab.com/mecbetse/betsee/tree/master

.. # ------------------( LINKS ~ mecbetse                   )------------------
.. _Alessandro Leronni:
   https://www.researchgate.net/profile/Alessandro_Leronni
.. _codebase:
   https://gitlab.com/mecbetse/mecbetse/tree/master
.. _conda package:
   https://anaconda.org/conda-forge/mecbetse
.. _contributors:
   https://gitlab.com/mecbetse/mecbetse/graphs/master
.. _issue submission:
   https://gitlab.com/mecbetse/mecbetse/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=
.. _issue tracker:
   https://gitlab.com/mecbetse/mecbetse/issues
.. _project:
   https://gitlab.com/mecbetse/mecbetse
.. _tarballs:
   https://gitlab.com/mecbetse/mecbetse/tags

.. # ------------------( LINKS ~ mecbetse : ci              )------------------
.. _Appveyor:
   https://ci.appveyor.com/project/mecbetse/mecbetse/branch/master
.. _testing:
   https://gitlab.com/mecbetse/mecbetse/pipelines

.. # ------------------( LINKS ~ mecbetse : local           )------------------
.. _author list:
   doc/md/AUTHORS.md
.. _license:
   LICENSE

.. # ------------------( LINKS ~ mecbetse : local : install )------------------
.. _install:
   doc/rst/INSTALL.rst
.. _install developers:
   doc/rst/INSTALL.rst#developers
.. _install Docker:
   doc/rst/INSTALL.rst#docker
.. _install Linux:
   doc/rst/INSTALL.rst#linux
.. _install macOS:
   doc/rst/INSTALL.rst#macos
.. _install Windows:
   doc/rst/INSTALL.rst#windows

.. # ------------------( LINKS ~ mecbetse : local : tutorial )------------------
.. _tutorial dev:
   doc/md/DEV_DEMO.md
.. _tutorial user:
   doc/md/TUTORIALS.md

.. # ------------------( LINKS ~ academia                   )------------------
.. _Michael Levin:
.. _Levin, Michael:
   https://ase.tufts.edu/biology/labs/levin
.. _Channelpedia:
   http://channelpedia.epfl.ch
.. _Paul Allen Discovery Center:
   http://www.alleninstitute.org/what-we-do/frontiers-group/discovery-centers/allen-discovery-center-tufts-university
.. _Paul Allen Discovery Center award:
   https://www.alleninstitute.org/what-we-do/frontiers-group/news-press/press-resources/press-releases/paul-g-allen-frontiers-group-announces-allen-discovery-center-tufts-university
.. _Paul G. Allen Frontiers Group:
   https://www.alleninstitute.org/what-we-do/frontiers-group
.. _Tufts University:
   https://www.tufts.edu

.. # ------------------( LINKS ~ academia : ally            )------------------
.. _Alexis Pietak:
.. _Pietak, Alexis:
.. _Dr. Pietak:
   https://www.researchgate.net/profile/Alexis_Pietak
.. _Organic Mechanics:
   https://www.omecha.org
.. _Organic Mechanics Contact:
   https://www.omecha.org/contact

.. # ------------------( LINKS ~ paper : 2016               )------------------
.. _2016 article:
   http://journal.frontiersin.org/article/10.3389/fbioe.2016.00055/abstract

.. |2016 article name| replace::
   **Exploring instructive physiological signaling with the bioelectric tissue
   simulation engine (mecBETSE).**
.. _2016 article name:
   http://journal.frontiersin.org/article/10.3389/fbioe.2016.00055/abstract

.. |2016 article supplement| replace::
   **(**\ Supplement\ **).**
.. _2016 article supplement:
   https://www.frontiersin.org/articles/file/downloadfile/203679_supplementary-materials_datasheets_1_pdf/octet-stream/Data%20Sheet%201.PDF/1/203679

.. |2016 journal name| replace::
   *Frontiers in Bioengineering and Biotechnology,*
.. _2016 journal name:
   http://journal.frontiersin.org/journal/bioengineering-and-biotechnology

.. # ------------------( LINKS ~ paper ~ 2017               )------------------
.. |2017 article name| replace::
   **Bioelectric gene and reaction networks: computational modelling of genetic, biochemical and bioelectrical dynamics in pattern regulation.**
.. _2017 article name:
   http://rsif.royalsocietypublishing.org/content/14/134/20170425

.. |2017 article supplement| replace::
   **(**\ Supplement\ **).**
.. _2017 article supplement:
   https://figshare.com/collections/Supplementary_material_from_Bioelectric_gene_and_reaction_networks_computational_modelling_of_genetic_biochemical_and_bioelectrical_dynamics_in_pattern_regulation_/3878404

.. |2017 journal name| replace::
   *Journal of The Royal Society Interface,*
.. _2017 journal name:
   http://rsif.royalsocietypublishing.org

.. # ------------------( LINKS ~ paper ~ 2018 : hcn2        )------------------
.. |2018 hcn2 article name| replace::
   **HCN2 rescues brain defects by enforcing endogenous voltage pre-patterns.**
.. _2018 hcn2 article name:
   https://www.nature.com/articles/s41467-018-03334-5

.. |2018 hcn2 article supplement| replace::
   **(**\ Supplement\ **).**
.. _2018 hcn2 article supplement:
   https://static-content.springer.com/esm/art%3A10.1038%2Fs41467-018-03334-5/MediaObjects/41467_2018_3334_MOESM1_ESM.pdf

.. |2018 hcn2 journal name| replace::
   *Nature Communications.*
.. _2018 hcn2 journal name:
   https://www.nature.com

.. # ------------------( LINKS ~ paper ~ 2018 : coupling    )------------------
.. |2018 coupling article name| replace::
   **Bioelectrical coupling in multicellular domains regulated by gap junctions: A conceptual approach.**
.. _2018 coupling article name:
   https://www.sciencedirect.com/science/article/pii/S156753941830063X?via%3Dihub

.. |2018 coupling journal name| replace::
   *Bioelectrochemistry.*
.. _2018 coupling journal name:
   https://www.sciencedirect.com/journal/bioelectrochemistry

.. # ------------------( LINKS ~ paper ~ 2018 : review      )------------------
.. |2018 review article name| replace::
   **Bioelectrical control of positional information in development and regeneration: A review of conceptual and computational advances.**
.. _2018 review article name:
   https://www.sciencedirect.com/science/article/pii/S0079610718300415

.. |2018 review journal name| replace::
   *Progress in Biophysics and Molecular Biology.*
.. _2018 review journal name:
   https://www.sciencedirect.com/journal/progress-in-biophysics-and-molecular-biology

.. # ------------------( LINKS ~ paper ~ 2019               )------------------
.. |2019 article name| replace::
   **Neural control of body-plan axis in regenerating planaria.**
.. _2019 article name:
   https://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1006904

.. |2019 journal name| replace::
   *PLOS Computational Biology.*
.. _2019 journal name:
   https://journals.plos.org/ploscompbiol

.. # ------------------( LINKS ~ science                    )------------------
.. _bioelectricity:
   https://en.wikipedia.org/wiki/Bioelectromagnetics
.. _biochemical reaction networks:
   http://www.nature.com/subjects/biochemical-reaction-networks
.. _discrete exterior calculus:
   https://en.wikipedia.org/wiki/Discrete_exterior_calculus
.. _electrodiffusion:
   https://en.wikipedia.org/wiki/Nernst%E2%80%93Planck_equation
.. _electro-osmosis:
   https://en.wikipedia.org/wiki/Electro-osmosis
.. _enzyme activity:
   https://en.wikipedia.org/wiki/Enzyme_assay
.. _ephaptic coupling:
   https://en.wikipedia.org/wiki/Ephaptic_coupling
.. _epigenetics:
   https://en.wikipedia.org/wiki/Epigenetics
.. _extracellular environment:
   https://en.wikipedia.org/wiki/Extracellular
.. _finite volume:
   https://en.wikipedia.org/wiki/Finite_volume_method
.. _galvanotaxis:
   https://en.wiktionary.org/wiki/galvanotaxis
.. _gap junction:
.. _gap junctions:
   https://en.wikipedia.org/wiki/Gap_junction
.. _gene products:
   https://en.wikipedia.org/wiki/Gene_product
.. _gene regulatory networks:
   https://en.wikipedia.org/wiki/Gene_regulatory_network
.. _genetics:
   https://en.wikipedia.org/wiki/Genetics
.. _genetic algorithms:
   https://en.wikipedia.org/wiki/Genetic_algorithm
.. _Hodgkin-Huxley (HH) formalism:
   https://en.wikipedia.org/wiki/Hodgkin%E2%80%93Huxley_model
.. _local field potentials:
   https://en.wikipedia.org/wiki/Local_field_potential
.. _membrane permeability:
   https://en.wikipedia.org/wiki/Cell_membrane
.. _resting potential:
   https://en.wikipedia.org/wiki/Resting_potential
.. _tight junctions:
   https://en.wikipedia.org/wiki/Tight_junction
.. _transmembrane voltage:
   https://en.wikipedia.org/wiki/Membrane_potential
.. _transepithelial potential:
   https://en.wikipedia.org/wiki/Transepithelial_potential_difference

.. # ------------------( LINKS ~ science : ions             )------------------
.. _anionic proteins:
   https://en.wikipedia.org/wiki/Ion#anion
.. _bicarbonate: https://en.wikipedia.org/wiki/Bicarbonate
.. _calcium:     https://en.wikipedia.org/wiki/Calcium_in_biology
.. _chloride:    https://en.wikipedia.org/wiki/Chloride
.. _hydrogen:    https://en.wikipedia.org/wiki/Hydron_(chemistry)
.. _sodium:      https://en.wikipedia.org/wiki/Sodium_in_biology
.. _potassium:   https://en.wikipedia.org/wiki/Potassium_in_biology

.. # ------------------( LINKS ~ science : channels         )------------------
.. _ion channel:
   https://en.wikipedia.org/wiki/Ion_channel
.. _leak channels:
   https://en.wikipedia.org/wiki/Leak_channel
.. _ligand-gated channels:
   https://en.wikipedia.org/wiki/Ligand-gated_ion_channel
.. _voltage-gated ion channels:
   https://en.wikipedia.org/wiki/Voltage-gated_ion_channel

.. |calcium-gated K+ channels| replace::
   Calcium-gated K\ :sup:`+` channels
.. _calcium-gated K+ channels:
   https://en.wikipedia.org/wiki/Calcium-activated_potassium_channel

.. # ------------------( LINKS ~ science : channels : type  )------------------
.. _HCN1:   http://channelpedia.epfl.ch/ionchannels/61
.. _HCN2:   http://channelpedia.epfl.ch/ionchannels/62
.. _HCN4:   http://channelpedia.epfl.ch/ionchannels/64
.. _Kir2.1: http://channelpedia.epfl.ch/ionchannels/42
.. _Kv1.1:  http://channelpedia.epfl.ch/ionchannels/1
.. _Kv1.2:  http://channelpedia.epfl.ch/ionchannels/2
.. _Kv1.5:  http://channelpedia.epfl.ch/ionchannels/5
.. _Kv3.3:  http://channelpedia.epfl.ch/ionchannels/13
.. _Kv3.4:  http://channelpedia.epfl.ch/ionchannels/14
.. _Nav1.2: http://channelpedia.epfl.ch/ionchannels/121
.. _Nav1.3: http://channelpedia.epfl.ch/ionchannels/122
.. _Nav1.6: http://channelpedia.epfl.ch/ionchannels/125
.. _L-type Ca:   http://channelpedia.epfl.ch/ionchannels/212
.. _T-type Ca:   https://en.wikipedia.org/wiki/T-type_calcium_channel

.. |P/Q-type Ca| replace:: :sup:`P`\ /\ :sub:`Q`-type Ca
.. _P/Q-type Ca:
   http://channelpedia.epfl.ch/ionchannels/78

.. # ------------------( LINKS ~ science : pumps : type     )------------------
.. _ion pumps:
   https://en.wikipedia.org/wiki/Active_transport

.. # ------------------( LINKS ~ science : pumps : type     )------------------
.. _V-ATPase: https://en.wikipedia.org/wiki/V-ATPase

.. |Ca2+-ATPase| replace:: Ca\ :sup:`2+`-ATPase
.. _Ca2+-ATPase: https://en.wikipedia.org/wiki/Calcium_ATPase

.. |H+/K+-ATPase| replace:: H\ :sup:`+`/K\ :sup:`+`-ATPase
.. _H+/K+-ATPase: https://en.wikipedia.org/wiki/Hydrogen_potassium_ATPase

.. |Na+/K+-ATPase| replace:: Na\ :sup:`+`/K\ :sup:`+`-ATPase
.. _Na+/K+-ATPase: https://en.wikipedia.org/wiki/Na%2B/K%2B-ATPase

.. # ------------------( LINKS ~ science : computer         )------------------
.. _Big Data:
   https://en.wikipedia.org/wiki/Big_data
.. _comma-separated values:
   https://en.wikipedia.org/wiki/Comma-separated_values
.. _continuous integration:
   https://en.wikipedia.org/wiki/Continuous_integration
.. _directed graphs:
   https://en.wikipedia.org/wiki/Directed_graph
.. _e-mail harvesting:
   https://en.wikipedia.org/wiki/Email_address_harvesting
.. _genenic algorithms:
   https://en.wikipedia.org/wiki/Genetic_algorithm
.. _knowledge-based systems:
   https://en.wikipedia.org/wiki/Knowledge-based_systems
.. _smoke test:
   https://en.wikipedia.org/wiki/Smoke_testing_(software)

.. # ------------------( LINKS ~ os : linux                 )------------------
.. _APT:
   https://en.wikipedia.org/wiki/Advanced_Packaging_Tool
.. _POSIX:
   https://en.wikipedia.org/wiki/POSIX
.. _Ubuntu:
.. _Ubuntu Linux:
   https://www.ubuntu.com
.. _Ubuntu Linux 16.04 (Xenial Xerus):
   http://releases.ubuntu.com/16.04

.. # ------------------( LINKS ~ os : macos                 )------------------
.. _Homebrew:
   http://brew.sh
.. _MacPorts:
   https://www.macports.org

.. # ------------------( LINKS ~ os : windows               )------------------
.. _WSL:
   https://msdn.microsoft.com/en-us/commandline/wsl/install-win10

.. # ------------------( LINKS ~ soft                       )------------------
.. _Atom:
   https://atom.io
.. _dill:
   https://pypi.python.org/pypi/dill
.. _FFmpeg:
   https://ffmpeg.org
.. _Git:
   https://git-scm.com/downloads
.. _GitLab-CI:
   https://about.gitlab.com/gitlab-ci
.. _Graphviz:
   http://www.graphviz.org
.. _Libav:
   https://libav.org
.. _MEncoder:
   https://en.wikipedia.org/wiki/MEncoder
.. _VirtualBox:
   https://www.virtualbox.org
.. _YAML:
   http://yaml.org

.. # ------------------( LINKS ~ soft : py                  )------------------
.. _imageio:
   https://imageio.github.io
.. _Matplotlib:
   http://matplotlib.org
.. _NumPy:
   http://www.numpy.org
.. _Python 3:
   https://www.python.org
.. _pip:
   https://pip.pypa.io
.. _py.test:
   http://pytest.org
.. _SciPy:
   http://www.scipy.org

.. # ------------------( LINKS ~ soft : py : conda          )------------------
.. _Anaconda:
   https://www.anaconda.com/download
.. _Anaconda packages:
   https://anaconda.org
.. _conda-forge:
   https://conda-forge.org

.. # ------------------( LINKS ~ soft : py : pyside2        )------------------
.. _PySide2:
   https://wiki.qt.io/PySide2
.. _PySide2 5.6:
   https://code.qt.io/cgit/pyside/pyside.git/log/?h=5.6
.. _PySide2 installation:
   https://wiki.qt.io/PySide2_GettingStarted
.. _PySide2 PPA:
   https://launchpad.net/~thopiekar/+archive/ubuntu/pyside-git
.. _Qt:
   https://www.qt.io
.. _Qt 5.6:
   https://wiki.qt.io/Qt_5.6_Release

.. # ------------------( LINKS ~ soft : icon                )------------------
.. _Cows collection:
   https://thenounproject.com/maxim221/collection/cows
.. _Maxim Kulikov:
   https://thenounproject.com/maxim221
.. _Noun Project:
   https://thenounproject.com
.. _Noun Project license:
   https://thenounproject.com/legal

.. # ------------------( LINKS ~ soft : license             )------------------
.. _license compatibility:
   https://en.wikipedia.org/wiki/License_compatibility#Compatibility_of_FOSS_licenses
.. _BSD 2-clause license:
   https://opensource.org/licenses/BSD-2-Clause
.. _CC BY 3.0 license:
   https://creativecommons.org/licenses/by/3.0
