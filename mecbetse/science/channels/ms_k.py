'''
Mechanosensitive channel classes.
'''

# .................... IMPORTS                            ....................
from abc import ABCMeta, abstractmethod
import numpy as np
from mecbetse.science import sim_toolbox as stb
from mecbetse.science.channels.mschannelsabc import MsChannelsABC
from mecbetse.science.math import toolbox as tb
from mecbetse.util.io.log import logs

# .................... BASE                               ....................
class MsKABC(MsChannelsABC, metaclass=ABCMeta):
    '''
    Abstract base class of all mechanosensitive channel classes.

    '''

    def init(self, tens_m_n, cells, p, targets = None):
        '''
        Initialize targeted mechanosensitive channels at the initial
        time step of the simulation based on the initial membrane tension (tens_m_n).

        '''

        if targets is None:

            self.targets = None

            if type(tens_m_n) == float or type(tens_m_n) == np.float64:
                self.data_length = 1

            else:
                self.data_length = len(tens_m_n)


        else:
            self.targets = targets
            self.data_length = len(self.targets)
            self.mdl = len(cells.mem_i)

        self.modulator = 1.0

        self.t_corr = 0.0

        if self.targets is None:

            tens_m_n = tens_m_n + self.t_corr

        else:
            tens_m_n = tens_m_n[self.targets] + self.t_corr

        self._init_state(tens_m_n)

        self.ions = ['K', 'Na'] # ions with respect to which mechanosensitive channels are selective
        self.rel_perm = [1.0, self._pmNa] # mechanosensitive channels are always selective to potassium, if desired also to sodium

    def run(self, tens_m_n, p):
        '''
        Handle all targeted mechanosensitive channels by working with the passed
        user-specified parameters on the tissue simulation and cellular
        world for a time step.

        '''

        if self.targets is None:

            tens_m_n = tens_m_n + self.t_corr

        else:

            tens_m_n = tens_m_n[self.targets] + self.t_corr


        self._calculate_state(tens_m_n)

        self._implement_state(tens_m_n, p)

    def _implement_state(self, tens_m_n, p):

        # Update the 'm' and 'h' gating functions:
        self.update_mh(p, time_unit = self.time_unit)

        # Calculate the open-probability of the channel:
        P = (self.m ** self._mpower) * (self.h ** self._hpower)

        if self.targets is None:

            self.P = P

        else:
            self.P = np.zeros(self.mdl)
            self.P[self.targets] = P


    @abstractmethod
    def _init_state(self, tens_m_n):
        '''
        Initializes values of the m and h gates of the channel.
        '''
        pass

    @abstractmethod
    def _calculate_state(self, tens_m_n):
        '''
        Calculates time-dependent values of the m and h gates of the channel.
        '''
        pass

# ....................{ SUBCLASS                           }....................

class MsK(MsKABC):
    '''

    '''

    def _init_state(self, tens_m_n):
        """

        Run initialization calculation for m and h gates of the channel at starting membrane tension (tens_m_n) value.

        """

        # Parameters (they should be defined in the file parameters)

        Ro = 3.5e-9 # Open radius [m]
        Rc = 2.3e-9 # Closed radius [m]
        w0 = 1.85e-9 # Membrane semi-thickness [m]
        k = 1.38e-23 # Boltzmann constant [J/K]
        T = 310 # Temperature [K]
        beta = 1/(k*T) # [J]
        Kb = 21/beta # Membrane bending stiffness [Nm]
        Kt = 64e18/beta # Membrane through-the-thickness stiffness [N/m]
        K = np.sqrt(2)*(Kt**3*Kb/w0**6)**(1/4) # Membrane composite stiffness [N/m^2]
        W = 3.3e-9 # Hydrophobic length of the channel [m]
        U = W/2-w0 # Half the hydrophobic mismatch [m]
        tmcrit = K*U**2/(Ro+Rc) # Critical membrane tension [N/m]

        logs.log_info('You are using the MsK channel: MsK ')

        self.time_unit = 1.0e3

        # initialize values of the m and h gates of the channel based on m_inf and h_inf:
        self.m = 1/(1 + np.exp(beta*np.pi*(Ro**2-Rc**2)*(tmcrit-tens_m_n)))
        self.h = np.ones(self.data_length)

        # m IS DEFINED AS FOR THE VOLTAGE-GATED CHANNELS, WITH THE MEMBRANE TENSION REPLACING THE MEMBRANE VOLTAGE
        # h DOES NOT PLAY ANY ROLE

        # define the power of m and h gates used in the final channel state equation:
        self._mpower = 1
        self._hpower = 1

        # mpower IS SET EQUAL TO 1, SUCH THAT m CORRESPONDS TO THE OPEN PROBABILITY

        self._pmNa = 0.0 # channel permeability to sodium in comparison with the permeability to potassium
        # (0 means no permeability to sodium; 1 means permeabilities to sodium and potassium are the same;
        # potassium max permeability is defined in the config file)

    def _calculate_state(self, tens_m_n):
        """

        Update the state of m and h gates of the channel given their present value and present
        simulation membrane tension (tens_m_n).

        """

        # Parameters (they should be defined in the file parameters)

        Ro = 3.5e-9 # Open radius [m]
        Rc = 2.3e-9 # Closed radius [m]
        w0 = 1.85e-9 # Membrane semi-thickness [m]
        k = 1.38e-23 # Boltzmann constant [J/K]
        T = 310 # Temperature [K]
        beta = 1/(k*T) # [J]
        Kb = 21/beta # Membrane bending stiffness [Nm]
        Kt = 64e18/beta # Membrane through-the-thickness stiffness [N/m]
        K = np.sqrt(2)*(Kt**3*Kb/w0**6)**(1/4) # Membrane composite stiffness [N/m^2]
        W = 3.3e-9 # Hydrophobic length of the channel [m]
        U = W/2-w0 # Half the hydrophobic mismatch [m]
        tmcrit = K*U**2/(Ro+Rc) # Critical membrane tension [N/m]

        self._mInf = 1/(1 + np.exp(beta*np.pi*(Ro**2-Rc**2)*(tmcrit-tens_m_n)))
        self._mTau = np.ones(self.data_length)
        self._hInf = np.ones(self.data_length)
        self._hTau = np.ones(self.data_length)

        # mInf IS SET EQUAL TO m, AND mTau EQUAL TO 1, SUCH THAT m IS NOT UPDATED
        # (WE NEGLECT THE DYNAMICS OF THE CHANNEL, BUT m CHANGES WITH TIME BECAUSE IT DEPENDS ON THE MEMBRANE TENSION!)
