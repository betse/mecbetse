#!/usr/bin/env python3
# --------------------( LICENSE                            )--------------------
# Copyright 2014-2019 by Alexis Pietak & Cecil Curry.
# See "LICENSE" for further details.

'''
Facilities guaranteeing backward compatibility with prior file formats for
gene regulatory networks (GRNs).
'''

# ....................{ IMPORTS                            }....................
from mecbetse.util.io.log import logs
from mecbetse.util.type.types import type_check, MappingType
