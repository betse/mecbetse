import numpy as np

def Mechanics(sim, cells, t, p):

    # Here I solve the Cauchy-Navier equations at each time step
    # The inverse of the coefficient matrix A (which is not updated in time as in the second-order theory)
    # is defined once for all in cells.Ainvmech


    # Geometrical structures

    # Cell centres
    cell_centres = cells.cell_centres

    # Membranes surrounding a cell
    cell_to_mems = cells.cell_to_mems

    # Number of membranes surrounding a cell
    num_mems = cells.num_mems

    # Cell adjacent to a membrane
    cell_nn_i = cells.cell_nn_i

    # Membrane adjacent to a membrane
    nn_i = cells.nn_i

    # Environmental grid adjacent to a membrane
    map_mem2ecm = cells.map_mem2ecm

    # Environmental grid k corresponding to row i and column j
    map_ij2k = cells.map_ij2k

    # Number of cells in the cluster
    cdl = len(cells.cell_i)

    # Cells on the boundary
    bflags_cells = cells.bflags_cells

    # Membrane surface areas
    A_n = cells.A_n

    # Cell volumes
    V_m = cells.cell_vol

    # Distances between cell centers (xi direction)
    x_m_n = cells.x_m_n
    y_m_n = cells.y_m_n

    # Distances between cell vertexes (eta direction)
    x_m_n_n = cells.x_m_n_n
    y_m_n_n = cells.y_m_n_n

    # Inverse of the numerator of the Jacobians
    J_m_n_inv = cells.J_m_n_inv

    # Outward unit normals to membranes
    n_x_n = cells.n_x_n
    n_y_n = cells.n_y_n

    # Lambda and Mu
    Lam = cells.Lam
    Mu = cells.Mu


    # CONSTRUCTION OF THE KNOWN VECTOR b


    # Electrochemical parameters (they should be defined in the file parameters)

    eps0 = 8.85e-12 # Vacuum permittivity [F/m] or [C/(V*m)]
    epsr = 3 # Relative permittivity [-]

    R = 8.314 # Gas constant [J/(mol*K)]
    T = 310 # Temperature [K]
    c_0 = 300 # Initial osmotic concentration [mol/m^3]

    d_mem = 7.5e-9 # Membrane thickness [m]


    # Data

    # Electric field (macroscopic) in each cell
    E_x_m = sim.E_cell_x
    E_y_m = sim.E_cell_y

    # Electric field (macroscopic) in each environmental grid
    E_x_env = sim.E_env_x
    E_y_env = sim.E_env_y

    # Membrane potentials
    v_m_ave = sim.vm_ave

    # Environmental voltages
    v_env = sim.v_env

    # Concentrations in each cell
    c_Na_m = sim.cc_cells[sim.iNa]
    c_K_m = sim.cc_cells[sim.iK]
    c_P_m = sim.cc_cells[sim.iP]
    c_M_m = sim.cc_cells[sim.iM]
    # Osmotic concentration in each cell
    c_m = c_Na_m + c_K_m + c_P_m + c_M_m

    # Concentrations in each environmental grid
    c_Na_env = sim.cc_env[sim.iNa]
    c_K_env = sim.cc_env[sim.iK]
    c_P_env = sim.cc_env[sim.iP]
    c_M_env = sim.cc_env[sim.iM]
    # Osmotic concentration in each environmental grid
    c_env = c_Na_env + c_K_env + c_P_env + c_M_env


    # Array initialization

    # Electric field at the cell membranes (macroscopic)
    E_x_m_n = np.zeros([cdl,cdl])
    E_y_m_n = np.zeros([cdl,cdl])

    # Osmotic concentration at the cell membranes
    c_m_n = np.zeros([cdl,cdl])

    # Electrostatic body force
    bm_m = np.zeros([2*cdl])

    # Osmotic body force
    bo_m = np.zeros([2*cdl])


    # Computation of the electrostatic body force

    # Cycle on the cells
    for m in np.arange(0,cdl):

        # Cycle on the membranes surrounding the cell m
        for n in np.arange(num_mems[m]): # try to replace "np.arange(num_mems[m])" with "cell_to_mems[m]"

            # Electric field at the cell membranes

            # if cell_nn_i[cell_to_mems[m][n],1] == m:
            #
            #     E_x_m_n[m,cell_nn_i[cell_to_mems[m][n],1]] = 0.5*(E_x_env[map_ij2k[map_mem2ecm[cell_to_mems[m][n]],0],map_ij2k[map_mem2ecm[cell_to_mems[m][n]],1]] + E_x_m[m])
            #
            #     E_y_m_n[m,cell_nn_i[cell_to_mems[m][n],1]] = 0.5*(E_y_env[map_ij2k[map_mem2ecm[cell_to_mems[m][n]],0],map_ij2k[map_mem2ecm[cell_to_mems[m][n]],1]] + E_y_m[m])
            #
            # else:
            #
            #     E_x_m_n[m,cell_nn_i[cell_to_mems[m][n],1]] = 0.5*(E_x_m[cell_nn_i[cell_to_mems[m][n],1]] + E_x_m[m]) # try to replace "cell_nn_i[cell_to_mems[m][n],1]" with "cell_nn_i[n,1]"
            #
            #     E_y_m_n[m,cell_nn_i[cell_to_mems[m][n],1]] = 0.5*(E_y_m[cell_nn_i[cell_to_mems[m][n],1]] + E_y_m[m])

            # On the cluster boundary
            if cell_nn_i[cell_to_mems[m][n],1] == m:

                E_x_m_n[m,cell_nn_i[cell_to_mems[m][n],1]] = 0
                # - ((v_env[map_mem2ecm[cell_to_mems[m][n]]]-v_m_ave[m])/(2*d_mem))*n_x_n[m,cell_nn_i[cell_to_mems[m][n],1]]

                E_y_m_n[m,cell_nn_i[cell_to_mems[m][n],1]] = 0
                # - ((v_env[map_mem2ecm[cell_to_mems[m][n]]]-v_m_ave[m])/(2*d_mem))*n_y_n[m,cell_nn_i[cell_to_mems[m][n],1]]

            # On the interior boundary
            else:

                E_x_m_n[m,cell_nn_i[cell_to_mems[m][n],1]] = - ((v_m_ave[cell_nn_i[cell_to_mems[m][n],1]]-v_m_ave[m])/(2*d_mem))*n_x_n[m,cell_nn_i[cell_to_mems[m][n],1]]

                E_y_m_n[m,cell_nn_i[cell_to_mems[m][n],1]] = - ((v_m_ave[cell_nn_i[cell_to_mems[m][n],1]]-v_m_ave[m])/(2*d_mem))*n_y_n[m,cell_nn_i[cell_to_mems[m][n],1]]

            # Electrostatic body force

            # x component
            bm_m[2*m] = bm_m[2*m]     - eps0*A_n[m,cell_nn_i[cell_to_mems[m][n],1]]*(((epsr-0.5)*(E_x_m_n[m,cell_nn_i[cell_to_mems[m][n],1]])**2 - 0.5*(E_y_m_n[m,cell_nn_i[cell_to_mems[m][n],1]])**2)*n_x_n[m,cell_nn_i[cell_to_mems[m][n],1]] + epsr*E_x_m_n[m,cell_nn_i[cell_to_mems[m][n],1]]*E_y_m_n[m,cell_nn_i[cell_to_mems[m][n],1]]*n_y_n[m,cell_nn_i[cell_to_mems[m][n],1]])
            # y component
            bm_m[2*m+1] = bm_m[2*m+1] - eps0*A_n[m,cell_nn_i[cell_to_mems[m][n],1]]*(((epsr-0.5)*(E_y_m_n[m,cell_nn_i[cell_to_mems[m][n],1]])**2 - 0.5*(E_x_m_n[m,cell_nn_i[cell_to_mems[m][n],1]])**2)*n_y_n[m,cell_nn_i[cell_to_mems[m][n],1]] + epsr*E_x_m_n[m,cell_nn_i[cell_to_mems[m][n],1]]*E_y_m_n[m,cell_nn_i[cell_to_mems[m][n],1]]*n_x_n[m,cell_nn_i[cell_to_mems[m][n],1]])


    # Magnitude of the electrostatic force

    for m in np.arange(0,cdl):

        sim.bm_x_m[m] = bm_m[2*m]
        sim.bm_y_m[m] = bm_m[2*m+1]
        sim.bm_mag_m[m] = ((sim.bm_x_m[m])**2+(sim.bm_y_m[m])**2)**0.5


    # Computation of the osmotic body force

    # Cycle on the cells
    for m in np.arange(0,cdl):

        # Cycle on the membranes surrounding the cell m
        for n in np.arange(num_mems[m]):

            # Osmotic concentration at the cell membranes

            # On the cluster boundary
            if cell_nn_i[cell_to_mems[m][n],1] == m:

                c_m_n[m,cell_nn_i[cell_to_mems[m][n],1]] = (c_env[map_mem2ecm[cell_to_mems[m][n]]] - c_0) - (c_m[m] - c_0)*V_m[m]/max(V_m)
                # 0.5*(c_env[map_mem2ecm[cell_to_mems[m][n]]] + c_m[m])

            # On the interior boundary
            else:

                c_m_n[m,cell_nn_i[cell_to_mems[m][n],1]] = 0.5*((c_m[cell_nn_i[cell_to_mems[m][n],1]]-c_0)*V_m[cell_nn_i[cell_to_mems[m][n],1]]/max(V_m) + (c_m[m] - c_0)*V_m[m]/max(V_m))

            # Osmotic body force

            # x component
            bo_m[2*m] = bo_m[2*m]     + R*T*A_n[m,cell_nn_i[cell_to_mems[m][n],1]]*c_m_n[m,cell_nn_i[cell_to_mems[m][n],1]]*n_x_n[m,cell_nn_i[cell_to_mems[m][n],1]] # try to replace "300" with "c_0" defined before
            # y component
            bo_m[2*m+1] = bo_m[2*m+1] + R*T*A_n[m,cell_nn_i[cell_to_mems[m][n],1]]*c_m_n[m,cell_nn_i[cell_to_mems[m][n],1]]*n_y_n[m,cell_nn_i[cell_to_mems[m][n],1]]


    # Magnitude of the osmotic force

    for m in np.arange(0,cdl):

        sim.bo_x_m[m] = bo_m[2*m]
        sim.bo_y_m[m] = bo_m[2*m+1]
        sim.bo_mag_m[m] = ((sim.bo_x_m[m])**2+(sim.bo_y_m[m])**2)**0.5


    # Total body force

    b = bm_m + bo_m


    # Magnitude of the total body force

    for m in np.arange(0,cdl):

        sim.b_x_m[m] = b[2*m]
        sim.b_y_m[m] = b[2*m+1]
        sim.b_mag_m[m] = ((sim.b_x_m[m])**2+(sim.b_y_m[m])**2)**0.5


    # COMPUTATION OF THE DISPLACEMENT x IN EACH CELL

    x = np.dot(cells.Ainv,b)


    # Magnitude of the displacement

    for m in np.arange(0,cdl):

        sim.u_x_m[m] = x[2*m]
        sim.u_y_m[m] = x[2*m+1]
        sim.u_mag_m[m] = ((sim.u_x_m[m])**2+(sim.u_y_m[m])**2)**0.5


    # COMPUTATION OF THE STRAIN FIELD

    # Here I compute the derivatives of the displacement components on each cell face,
    # and then the values in the cell center as the average of them


    # Initialization

    u_xx_m_n = np.zeros([cdl,cdl])
    u_xy_m_n = np.zeros([cdl,cdl])
    u_yx_m_n = np.zeros([cdl,cdl])
    u_yy_m_n = np.zeros([cdl,cdl])

    u_xx_m = np.zeros([cdl])
    u_xy_m = np.zeros([cdl])
    u_yx_m = np.zeros([cdl])
    u_yy_m = np.zeros([cdl])


    # Cycle on the cells
    for m in np.arange(0,cdl):

        # Cycle on the membranes surrounding the cell m
        for n in np.arange(num_mems[m]):

            # Displacement derivatives at the membrane-mids
            if n<num_mems[m]-1:

                u_xx_m_n[m,cell_nn_i[cell_to_mems[m][n],1]] = J_m_n_inv[m,cell_nn_i[cell_to_mems[m][n],1]]*(y_m_n_n[m,cell_nn_i[cell_to_mems[m][n],1]]*(sim.u_x_m[cell_nn_i[cell_to_mems[m][n],1]] - sim.u_x_m[m]) - y_m_n[m,cell_nn_i[cell_to_mems[m][n],1]]*(sim.u_x_m[cell_nn_i[cell_to_mems[m][n+1],1]] - sim.u_x_m[cell_nn_i[cell_to_mems[m][n-1],1]])/3)
                u_xy_m_n[m,cell_nn_i[cell_to_mems[m][n],1]] = J_m_n_inv[m,cell_nn_i[cell_to_mems[m][n],1]]*(-x_m_n_n[m,cell_nn_i[cell_to_mems[m][n],1]]*(sim.u_x_m[cell_nn_i[cell_to_mems[m][n],1]] - sim.u_x_m[m]) + x_m_n[m,cell_nn_i[cell_to_mems[m][n],1]]*(sim.u_x_m[cell_nn_i[cell_to_mems[m][n+1],1]] - sim.u_x_m[cell_nn_i[cell_to_mems[m][n-1],1]])/3)
                u_yx_m_n[m,cell_nn_i[cell_to_mems[m][n],1]] = J_m_n_inv[m,cell_nn_i[cell_to_mems[m][n],1]]*(y_m_n_n[m,cell_nn_i[cell_to_mems[m][n],1]]*(sim.u_y_m[cell_nn_i[cell_to_mems[m][n],1]] - sim.u_y_m[m]) - y_m_n[m,cell_nn_i[cell_to_mems[m][n],1]]*(sim.u_y_m[cell_nn_i[cell_to_mems[m][n+1],1]] - sim.u_y_m[cell_nn_i[cell_to_mems[m][n-1],1]])/3)
                u_yy_m_n[m,cell_nn_i[cell_to_mems[m][n],1]] = J_m_n_inv[m,cell_nn_i[cell_to_mems[m][n],1]]*(-x_m_n_n[m,cell_nn_i[cell_to_mems[m][n],1]]*(sim.u_y_m[cell_nn_i[cell_to_mems[m][n],1]] - sim.u_y_m[m]) + x_m_n[m,cell_nn_i[cell_to_mems[m][n],1]]*(sim.u_y_m[cell_nn_i[cell_to_mems[m][n+1],1]] - sim.u_y_m[cell_nn_i[cell_to_mems[m][n-1],1]])/3)

            else:

                u_xx_m_n[m,cell_nn_i[cell_to_mems[m][n],1]] = J_m_n_inv[m,cell_nn_i[cell_to_mems[m][n],1]]*(y_m_n_n[m,cell_nn_i[cell_to_mems[m][n],1]]*(sim.u_x_m[cell_nn_i[cell_to_mems[m][n],1]] - sim.u_x_m[m]) - y_m_n[m,cell_nn_i[cell_to_mems[m][n],1]]*(sim.u_x_m[cell_nn_i[cell_to_mems[m][0],1]] - sim.u_x_m[cell_nn_i[cell_to_mems[m][n-1],1]])/3)
                u_xy_m_n[m,cell_nn_i[cell_to_mems[m][n],1]] = J_m_n_inv[m,cell_nn_i[cell_to_mems[m][n],1]]*(-x_m_n_n[m,cell_nn_i[cell_to_mems[m][n],1]]*(sim.u_x_m[cell_nn_i[cell_to_mems[m][n],1]] - sim.u_x_m[m]) + x_m_n[m,cell_nn_i[cell_to_mems[m][n],1]]*(sim.u_x_m[cell_nn_i[cell_to_mems[m][0],1]] - sim.u_x_m[cell_nn_i[cell_to_mems[m][n-1],1]])/3)
                u_yx_m_n[m,cell_nn_i[cell_to_mems[m][n],1]] = J_m_n_inv[m,cell_nn_i[cell_to_mems[m][n],1]]*(y_m_n_n[m,cell_nn_i[cell_to_mems[m][n],1]]*(sim.u_y_m[cell_nn_i[cell_to_mems[m][n],1]] - sim.u_y_m[m]) - y_m_n[m,cell_nn_i[cell_to_mems[m][n],1]]*(sim.u_y_m[cell_nn_i[cell_to_mems[m][0],1]] - sim.u_y_m[cell_nn_i[cell_to_mems[m][n-1],1]])/3)
                u_yy_m_n[m,cell_nn_i[cell_to_mems[m][n],1]] = J_m_n_inv[m,cell_nn_i[cell_to_mems[m][n],1]]*(-x_m_n_n[m,cell_nn_i[cell_to_mems[m][n],1]]*(sim.u_y_m[cell_nn_i[cell_to_mems[m][n],1]] - sim.u_y_m[m]) + x_m_n[m,cell_nn_i[cell_to_mems[m][n],1]]*(sim.u_y_m[cell_nn_i[cell_to_mems[m][0],1]] - sim.u_y_m[cell_nn_i[cell_to_mems[m][n-1],1]])/3)

        # Displacement derivatives in the cell centers

            u_xx_m[m] = sum(u_xx_m_n[m,:])/num_mems[m]
            u_xy_m[m] = sum(u_xy_m_n[m,:])/num_mems[m]
            u_yx_m[m] = sum(u_yx_m_n[m,:])/num_mems[m]
            u_yy_m[m] = sum(u_yy_m_n[m,:])/num_mems[m]


    # Small strain tensor components

    sim.eps_xx_m = u_xx_m
    sim.eps_yy_m = u_yy_m
    sim.eps_xy_m = 0.5*(u_xy_m + u_yx_m)
    sim.eps_zz_m = - (Lam[m]/(2*Mu[m]))*(sim.eps_xx_m + sim.eps_yy_m) # COMMENT UNDER PLANE STRAIN


    # Volumetric deformation

    sim.eps_vol_m = sim.eps_xx_m + sim.eps_yy_m + sim.eps_zz_m


    # COMPUTATION OF THE STRESS FIELD

    # Cauchy stress components

    # Cycle on the cells
    for m in np.arange(0,cdl):

        sim.sig_xx_m[m] = (2*Mu[m]+Lam[m])*sim.eps_xx_m[m] + Lam[m]*sim.eps_yy_m[m]
        sim.sig_yy_m[m] = (2*Mu[m]+Lam[m])*sim.eps_yy_m[m] + Lam[m]*sim.eps_xx_m[m]
        sim.sig_xy_m[m] = 2*Mu[m]*sim.eps_xy_m[m]
        # sim.sig_zz_m[m] = Lam[m]*(sim.eps_xx_m[m] + sim.eps_yy_m[m]) # COMMENT UNDER PLANE STRESS


    # Pressure

    sim.press_m = (sim.sig_xx_m + sim.sig_yy_m + sim.sig_zz_m)/3


    # COMPUTATION OF THE MEMBRANE TENSION (YOUNG-LAPLACE LAW)

    Rc = 5e-6 # Cell radius [m] # try to replace with the average of the distances between the cell center and the vertexes (especially for a non-regular grid)

    # Membrane tension in the cell centers
    sim.tens_m = sim.press_m*Rc

    # Membrane tension at the mem-mids

    # Cycle on the cells
    for m in np.arange(0,cdl):

        sim.tens_m_n[cell_to_mems[m]]=sim.tens_m[m]
